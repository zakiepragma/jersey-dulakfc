import { Box, Container, Divider } from "@material-ui/core";
import BottomAppBar from "./components/BottomAppBar";
import TableData from "./components/TableData";

function App() {
  return (
    <>
      <Container>
        <Box component="span" m={20} className="center">
          <h1>Jersey Bola</h1>
        </Box>
        <Divider/>
        <TableData/>
      </Container>
      <BottomAppBar/>
    </>
  );
}

export default App;
