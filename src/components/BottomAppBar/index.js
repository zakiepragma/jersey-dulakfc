import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  appBar: {
    top: 'auto',
    bottom: 0,
  },
  fabButton: {
    position: 'absolute',
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: '0 auto',
  },
  grow: {
    flexGrow: 1,
  },
}));

const BottomAppBar = () => {

  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [form, setForm] = useState('Tambah Pemain');
  const [actionName, setActionName] = useState('Simpan Data');

  const [nama, setNama] = useState("");
  const [namapunggung, setNamaPunggung] = useState('');
  const [nomorpunggung, setNomorPunggung] = useState('');
  const [bayar, setBayar] = useState(0);

  const [errorNama, setErrorNama] = useState(false)
  const [errorNamaP, setErrorNamaP] = useState(false)
  const [errorNomorP, setErrorNomorP] = useState(false)

  const [valid, setValid] = useState(true);
  const [loading, setLoading] = useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async(e) => {
    e.preventDefault();

    if(nama==""){
      setErrorNama(true);
      setValid(false);
    }else{
      setErrorNama(false);
      setValid(true)
    }
    if(namapunggung==""){
      setErrorNamaP(true);
      setValid(false)
    }else{
      setErrorNamaP(false);
      setValid(true)
    }
    if(nomorpunggung==""){
      setErrorNomorP(true);
      setValid(false)
    }else{
      setErrorNomorP(false);
      setValid(true)
    }

    if(valid){
      const jersey = {
        nama:nama,
        namapunggung:namapunggung,
        nomorpunggung:nomorpunggung,
      };
        setLoading(true)
        try {
          const res = await axios.post(`https://programmercintasunnah.github.io/jersey_dulak_api/db.json`, jersey);
          console.log(res.data)
        } catch (e) {
         alert(e)
        }
        // setLoading(false)
        // setOpen(false)
    }
  }

  return (
    <AppBar position="fixed" color='default' className={classes.appBar}>
        <Toolbar>
        <IconButton edge="start" color="primary">
            <Button variant="contained" color="primary" href="https://gitlab.com/zakiepragma/jersey-dulakfc" target="_blank">Download</Button>
          </IconButton>
        <Fab color="default" type='button' onClick={()=>{handleClickOpen()}} aria-label="add" className={classes.fabButton}>
            <AddIcon/>
        </Fab>
        <div className={classes.grow} />
          <IconButton edge="end" color="secondary">
            <Button variant="contained" color="secondary" href="https://programmercintasunnah.github.io/jersey_dulak_api/db.json" target="_blank">Show API</Button>
          </IconButton>
        </Toolbar>

        <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {form}
        </DialogTitle>
        <form onSubmit={handleSubmit} className={classes.root} noValidate autoComplete="off">
        <DialogContent dividers>
            <TextField error={errorNama} value={nama} onChange={(e)=>setNama(e.target.value)} label="Nama" variant="outlined" fullWidth style={{ margin: 8 }}/>
            <TextField error={errorNamaP} value={namapunggung} onChange={(e)=>setNamaPunggung(e.target.value)} label="Nama Punggung" variant="outlined" fullWidth style={{ margin: 8 }}/>
            <TextField error={errorNomorP} value={nomorpunggung} onChange={(e)=>setNomorPunggung(e.target.value)} label="Nomor Punggung" variant="outlined" fullWidth style={{ margin: 8 }}/>
            <TextField value={bayar} onChange={(e)=>setBayar(e.target.value)} label="Bayar" disabled variant="outlined" fullWidth style={{ margin: 8 }}/>
        </DialogContent>
        <DialogActions>
          <Button type='submit' autoFocus color="secondary">
            {actionName}
          </Button>
          <Button autoFocus onClick={handleClose} color="primary">
            Batal
          </Button>
        </DialogActions>
        </form>
      </Dialog>

    </AppBar>
  )
}

export default BottomAppBar