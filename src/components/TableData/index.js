import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Avatar, Chip } from '@material-ui/core';
import axios from 'axios';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const TableData = () => {
  
  const url = "https://programmercintasunnah.github.io/jersey_dulak_api/db.json";

  const [loading, setLoading] = useState(true);
  const [data,setData] = useState([]);

  const classes = useStyles();

  useEffect(()=>{
    const fectData = async () => {
      setLoading(true);
      try {
        const {data: response} = await axios.get(url);
        setData(response)
        console.log(response)
      } catch (error) {
        console.log(error.message)
      }
      setLoading(false)
    }
    fectData();
  },[])

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Nama</TableCell>
            <TableCell align="center">Nama Punggung</TableCell>
            <TableCell align="center">Nomor Punggung</TableCell>
            <TableCell align="center">Bayar</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row) => (
            <TableRow key={row.nama}>
              <TableCell component="th" scope="row">
                <Chip
                    avatar={<Avatar>{row.nama.charAt(0)}</Avatar>}
                    label={row.nama}
                    clickable
                    onClick={()=>console.log(row.nama)}
                    variant="outlined"
                    color={row.bayar>=150000?'primary':'secondary'}
                />
                
              </TableCell>
              <TableCell align="center">{row.namapunggung}</TableCell>
              <TableCell align="center">{row.nomorpunggung}</TableCell>
              <TableCell align="center">{row.bayar}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
export default TableData